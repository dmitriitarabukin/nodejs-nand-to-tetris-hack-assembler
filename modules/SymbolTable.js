"use strict";

var SymbolTable = {
	symbol_table: {
		"R0": "0",
		"R1": "1",
		"R2": "2",
		"R3": "3",
		"R4": "4",
		"R5": "5",
		"R6": "6",
		"R7": "7",
		"R8": "8",
		"R9": "9",
		"R10": "10",
		"R11": "11",
		"R12": "12",
		"R13": "13",
		"R14": "14",
		"R15": "15",
		"SCREEN": "16384",
		"KBD": "24576",
		"SP": "0",
		"LCL": "1",
		"ARG": "2",
		"THIS": "3",
		"THAT": "4"
	},
	mem: 16,

	tryAddLabel(label) {
		if (!this.symbolExists(label.name)) {
			this.addLabel(label);
		}
	}, 

	addLabel(label) {
		this.symbol_table[label.name] = label.instruction_number;
	},

	addVariable(variable) {
		this.symbol_table[variable] = (this.mem).toString();
		this.mem++;
	},

	symbolExists(symbol) {
		return Object.keys(this.symbol_table).some(function(name) {
			return name === symbol;
		});
	},

	convertAddress(instruction) {
		instruction.address = this.symbol_table[instruction.address];
		return instruction;
	},

	processAddress(instruction) {
		if (this.symbolExists(instruction.address)) {
			instruction = this.convertAddress(instruction);
		} else {
			this.addVariable(instruction.address);
			instruction = this.convertAddress(instruction);
		}
		return instruction;
	}
};

module.exports = SymbolTable;