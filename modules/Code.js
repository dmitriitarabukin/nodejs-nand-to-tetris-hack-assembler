"use strict";

var Code = {
	translate(instruction) {
		let out = "";
		// Set instruction type bit
		let type = this.type(instruction.type);
		if (instruction.type === "A") {
			let address = this.address(instruction.address);
			let padded_address = this.pad(address);
			out = type + padded_address;
		} else {
			let comp = this.comp(instruction.comp);
			let dest = this.dest(instruction.dest);
			let jump = this.jump(instruction.jump);
			out = type + "11" + comp + dest + jump;
		}		
		return out;
	},

	type(instruction_type) {
		let type;
		switch(instruction_type) {
			case "A":
				type = "0";
				break;
			case "C":
				type = "1";
				break;
		}
		return type;
	},

	address(instruction_address) {
		return Number(instruction_address).toString(2);
	},

	pad(address) {
		return address.padStart(15, "0");
	},

	comp(instruction_comp) {
		let comp = "";
		// Set the "a" bit of computed function
		switch(instruction_comp) {
			case null:
			case "0":
			case "1":
			case "-1":
			case "D":
			case "A":
			case "!D":
			case "!A":
			case "-D":
			case "-A":
			case "D+1":
			case "A+1":
			case "D-1":
			case "A-1":
			case "D+A":
			case "D-A":
			case "A-D":
			case "D&A":
			case "D|A":
				comp += 0;
				break;
			case "M":
			case "!M":
			case "-M":
			case "M+1":
			case "M-1":
			case "D+M":
			case "D-M":
			case "M-D":
			case "D&M":
			case "D|M":
				comp += 1;
				break;
		}
		// Set 6 bits of computed function
		switch(instruction_comp) {
			case null:
				comp += "000000";
				break;
			case "0":
				comp += "101010";
				break;
			case "1":
				comp += "111111";
				break;
			case "-1":
				comp += "111010";
				break;
			case "D":
				comp += "001100";
				break;
			case "A":
			case "M":
				comp += "110000";
				break;
			case "!D":
				comp += "001101";
				break;
			case "!A":
			case "!M":
				comp += "110001";
				break;
			case "-D":
				comp += "001111";
				break;
			case "-A":
			case "-M":
				comp += "110011";
				break;
			case "D+1":
				comp += "011111";
				break;
			case "A+1":
			case "M+1":
				comp += "110111";
				break;
			case "D-1":
				comp += "001110";
				break;
			case "A-1":
			case "M-1":
				comp += "110010";
				break;
			case "D+A":
			case "D+M":
				comp += "000010";
				break;
			case "D-A":
			case "D-M":
				comp += "010011";
				break;
			case "A-D":
			case "M-D":
				comp += "000111";
				break;
			case "D&A":
			case "D&M":
				comp += "000000";
				break;
			case "D|A":
			case "D|M":
				comp += "010101";
				break;
		}
		return comp;
	},

	dest(instruction_dest) {
		// Set destination bits
		let dest = "";
		switch(instruction_dest) {
			case null:
				dest += "000";
				break;
			case "M":
				dest += "001";
				break;
			case "D":
				dest += "010";
				break;
			case "MD":
				dest += "011";
				break;
			case "A":
				dest += "100";
				break;
			case "AM":
				dest += "101";
				break;
			case "AD":
				dest += "110";
				break;
			case "AMD":
				dest += "111";
				break;
		}
		return dest;
	},

	jump(instruction_jump) {
		// Set jump bits
		let jump = "";
		switch(instruction_jump) {
			case null:
				jump += "000";
				break;
			case "JGT":
				jump += "001";
				break;
			case "JEQ":
				jump += "010";
				break;
			case "JGE":
				jump += "011";
				break;
			case "JLT":
				jump += "100";
				break;
			case "JNE":
				jump += "101";
				break;
			case "JLE":
				jump += "110";
				break;
			case "JMP":
				jump += "111";
				break;
		}
		return jump;
	}
};

module.exports = Code;