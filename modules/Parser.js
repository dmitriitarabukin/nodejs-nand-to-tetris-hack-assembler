"use strict";

const fs = require("fs");
const line_reader = require("n-readlines");

var Parser = {
	liner: null,
	current_line: null,
	current_instruction_number: 0,
	init(filename) {
		this.liner = new line_reader(filename);
	},

	hasMoreCommands() {
		this.current_line = this.liner.next();
		return Boolean(this.current_line);
	},

	advance(is_first_pass) {
		if (is_first_pass) {
			return this.parseLabels();
		} else {
			return this.parseInstructions();
		}		
	},

	clearWhitespace() {
		// TODO: refactor to single regexp
		this.current_line = this.current_line.replace(/^\s*/, "");
		this.current_line = this.current_line.replace(/\s*$/, "");
		this.current_line = this.current_line.replace(/\s*\/\/.*/, "");
	},

	clearLabels() {
		this.current_line = this.current_line.replace(/^[(].*[)]$/);
	},

	address() {
		return this.current_line.match(/(?<=@).*/)[0];
	},

	comp(is_jump) {
		if (is_jump) {
			return this.current_line.match(/\w*|0(?=;)/)[0];
		} else {
			return this.current_line.match(/(?<==).*/)[0];
		}
	},

	dest() {
		return this.current_line.match(/\w*(?==)/)[0];
	},

	jump() {
		return this.current_line.match(/(?<=;)\w*/)[0];
	},

	parseLabels() {
		this.current_line = this.current_line.toString("utf8");
		this.clearWhitespace();
		if (/^\s*$/.test(this.current_line)) {
			return false;
		}
		if (!/^[(]/.test(this.current_line)) {
			this.current_instruction_number++;
		}
		// If label
		if (/^[(]/.test(this.current_line)) {
			return {
				name: this.current_line.match(/(?<=[(]).*(?=[)])/)[0],
				instruction_number: (this.current_instruction_number).toString() // Label points to the next line
			};
		}
	},

	parseInstructions() {
		this.current_line = this.current_line.toString("utf8");
		this.clearWhitespace();
		if (/^\s*$|^[(]/.test(this.current_line)) {
			return false;
		}
		// If A-Command
		if (/^@/.test(this.current_line)) {
			let address = this.address();
			return {
				type: "A",
				address: address
			};
		}
		// If C-Command
		else {
			let dest, comp, jump;			
			if (/^\w*=/.test(this.current_line)) {
				dest = this.dest();
				comp = this.comp(false);
				jump = null;
			} else if (/^\w*0?;/.test(this.current_line)) {
				dest = null;
				comp = this.comp(true);
				jump = this.jump();
			}
			return { 
				type: "C", 
				dest: dest, 
				comp: comp, 
				jump: jump 
			};
		}
	}
};

module.exports = Parser;