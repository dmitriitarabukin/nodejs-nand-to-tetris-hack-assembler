const fs = require("fs");
const path = require("path");
const Parser = require("./modules/Parser.js");
const Code = require("./modules/Code.js");
const SymbolTable = require("./modules/SymbolTable.js");

var file = process.argv[2];
var result = "";
var current_parsed_line;

// First pass
Parser.init(file);
while (Parser.hasMoreCommands()) {
	current_parsed_line = Parser.advance(true);
	if (current_parsed_line) {
		SymbolTable.tryAddLabel(current_parsed_line);
	}
}

// Second pass
Parser.init(file);
while (Parser.hasMoreCommands()) {
	current_parsed_line = Parser.advance(false);
	if (current_parsed_line) {
		if (current_parsed_line.type === "A" && Number.isNaN(Number(current_parsed_line.address))) {
			current_parsed_line = SymbolTable.processAddress(current_parsed_line);
		}
		result += Code.translate(current_parsed_line) + "\n";
	}
}

fs.writeFile("output/" + path.basename(file, ".asm") + ".hack", result, function(error) {
	if (error) throw error;
	console.log("Assembly completed.");
});