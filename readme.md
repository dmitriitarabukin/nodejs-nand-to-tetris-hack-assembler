# JSHackAssembler (Nand to Tetris)
Hack assembler for [Nand to Tetris course](https://www.coursera.org/learn/build-a-computer)

## Features
* Translates instructions written in Hack assembly language to Hack machine code

## How to use
* Clone project
* Install node.js
* Run npm install
* Run bash command: "node HackAssembler <path_to_assembly_file>"
* The output file could be found in output directory (test .asm files could be found in test_asm_files directory)
* In order to test the machine code, download the Hack CPU emulator and follow the instructions on 
[Nand to Tetris](https://www.nand2tetris.org/software) course page.